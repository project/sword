<?php

/**
 * @file
 * @brief
 * Implements SWORD repository protocol.
 *
 * Default SWORD protocol code for handling a deposit
 * but only for tagging (though might be extended).
 *
 * For further information on SWORD go to
 * http://www.ukoln.ac.uk/repositories/digirep/index/SWORD
 *
 * @update
 * Modified to allow various modules to process the received
 * deposit in their own way, who knows what they might want
 * to do. Basically fetch the node to be processed and send
 * that, and the received deposit, round the system.
 *
 * @package 	sword
 * @subpackage
 * @author
 */

/**
 * Rally:US368:TA248
 */

function _sword_process_xml($data, $xml) {
    try {
        $parser = sword_parse_xml($xml);
        $parser->parse();
    }
    catch (Exception $e) {
        watchdog('sword', '%message', array('%message' => $e->getMessage()));
        return;
    }

// -- find out what the node ID is, this utilises
// -- my clever parser which allows direct access
// -- to elements just by using their name, but colons
// -- (for namespaces) are shown with double underscores
// -- the returned value is actually an object but casting
// -- it to a string returns the element's value (cool).

    $entry = $parser->lom__entry;

    if ($entry !== FALSE) {
        $entry = array_shift($entry);
        $nid = (string) $entry;
    }
    else {
        watchdog('sword', t('Failed to find Node ID of returned node'));
        return;
    }


    $node = node_load($nid, NULL, TRUE); // clear the node cache
    $data = array('node' => &$node, 'parser' => &$parser); // Items cannot be passed by reference using module_invoke_all so we have to get clever with arrays

    try {
        module_invoke_all('sword', 'process', $data, 'banana');
    }
    catch (Exception $e) {
        watchdog('sword', '%message', array('%message' => $e->getMessage()));
        watchdog('sword', '%tracestring', array('%tracestring' => $e->getTraceAsString()));
        return;
    }

// -- having passed the node round, save it to store any changes

    $node->revision = TRUE; // FIXME: force revision, because it's not generated even it's enabled
    node_save($node);
}

