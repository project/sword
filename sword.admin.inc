<?php

/**
 * @file
 * @brief
 * Implements SWORD repository protocol.
 *
 * Implements the SWORD protocol in order to
 * be able to receive SWORD-based deposits.
 *
 * For further information on SWORD go to
 * http://www.ukoln.ac.uk/repositories/digirep/index/SWORD
 *
 * @package     sword
 * @subpackage
 * @author
 */

/**
 * Menu callback for the settings form.
 */
function sword_get_admin_form() {
  $form['sword'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['sword']['sword_depository_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Depository title'),
    '#description' => t('See: !url', array('!url' => l('RFC4287', 'http://tools.ietf.org/html/rfc4287#section-4.2.14'))),
    '#default_value' => variable_get('sword_depository_title', variable_get('site_name', 'Drupal')),
    '#required' => TRUE,
  );

  $form['sword']['sword_depository_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Depository name'),
    '#description' => t('See: !url', array('!url' => l('RFC4287', 'http://tools.ietf.org/html/rfc4287#section-4.2.14'))),
    '#default_value' => variable_get('sword_depository_name', variable_get('site_name', 'Drupal')),
    '#required' => TRUE,
  );

  $form['sword']['sword_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorisation key'),
    '#description' => t('SWORD auth key for accessing the SWORD server. Available tokens: !user, !pass'),
    '#default_value' => variable_get('sword_access_key', 'username=!name:password=!pass:nso'),
    '#required' => TRUE,
  );

  $form['sword']['sword_ignore_local'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ignore access checking for the same host.'),
    '#description' => t('Activate this, if you want to give access to request sent by the same host. Use it only for testing purposes.'),
    '#default_value' => variable_get('sword_ignore_local', FALSE),
  );

  return system_settings_form($form);
}


/**
 * Menu callback for the settings form.
 */
function sword_get_test_form() {
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['sword'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['sword']['sword_xml_file'] = array(
    '#type' => 'file',
    '#title' => t('Attach XML file'),
  );

  $form['sword']['sword_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Upload file',
  );
  $form['#attributes']['enctype'] = 'multipart/form-data';

  return $form;
}

/**
 * Menu callback for form validation
 */
function sword_get_test_form_validate($form, &$form_state) {
    $values = $form_state['values'];
}

/**
 * Menu callback for form submit
 */
function sword_get_test_form_submit($form, $form_state) {
    if ($file = file_save_upload("sword_xml_file")) {
        _sword_deposit($file->filepath, FALSE, FALSE);
        $nid = db_result(db_query('SELECT nid FROM {node} ORDER BY changed DESC LIMIT 1'));
        
        $message = t('File uploaded successfully. Updated node: !uri', array('!uri' => l($nid, "node/$nid")));
        drupal_set_message($message);
        
        drupal_goto('admin/settings/sword/test');
    } else {
        
        $message = t('File to import not found.');
        drupal_set_message($message, 'error');
        
        $form_state['redirect'] = 'admin/settings/sword/test';
        return;
    }
}

