<?php

/**
 *
 * @file
 * @brief Generic class-based XML Parser
 * @package cql
 * @subpackage
 * @author
 */

/**
 * swordXMLVisitor
 *
 * Abstract visitor class (see bottom for generic visitor implementations)
 */
abstract class swordXMLVisitor {
    /**
     * swordXMLVisitor::visitTag()
     *
     * @param swordXMLtag $tag
     * @param mixed $depth
     * @return
     */
    abstract function visitTag(swordXMLtag $tag, $depth);
}

/**
 * Class describing an XML tag and its relationship to a structure
 */

class swordXMLtag {
    private $name = '';
    private $attrs = array();
    private $data = '';
    private $kids = array();
    private $count = 0;

    /**
     * swordXMLtag::__construct()
     *
     * @param mixed $name
     * @param array $attrs
     */
    private function __construct($name, $attrs = array()) {
        $this->name = $name;
        $this->attrs = $attrs;
    }

    /**
     * When getting 'data' this code checks for the special case where the
     * data has been encased in a lom:string (which is then a child of the
     * node) and just gets that as if it were straight data.
     */
    public function __get($var) {
        switch ($var) {
            case 'data':
                if ($this->data == '') {
                    if (count($this->kids) == 1) {
                        if ($this->kids[0]->name == 'lom:string') {
                            $this->data = $this->kids[0]->data;
                        }
                    }
                }
                $val = $this->data;
                break;
            case 'name':
            case 'count':
                $val = $this->$var;
                break;
            default:
                if (array_key_exists($var, $this->attrs)) {
                    $val = $this->attrs[$var];
                } else {
                    $val = NULL;
                }
                break;
        }
        return $val;
    }

    /**
     * Allow more text to be added to the data field, and have the pseudo-field 'kid'
     * and assigning an object to this simply adds a new array element to the 'kids'
     * field (as long the supplied value is an array containing an object reference)
     * [which might not be necessary but I'm being careful]
     */

    public function __set($var, $val) {
        switch ($var) {
            case 'data':
                $this->data = $val;
                break;
            case 'kid':
                if (is_array($val) && is_object($val[0])) {
                    $this->kids[] = &$val[0];
                } else {
                    throw new Exception('Supplied child must be an object reference in an array in swordXMLTag object');
                }
                break;
            default:
                throw new Exception(t('Can\'t set unknown or private variable %var in swordXMLTag object', array('%var' => $var)));
        }
    }

    /**
     * Use the magic function to ensure we actually get the right 'data'
     * (ignoring any lom language string tag inserted)
     */
    public function __toString() {
        return $this->__get('data');
    }

    /**
     * Implement the visitor pattern by accepting visitors to the object
     * and sending any visitor off to any children the node might have.
     */
    public function accept(swordXMLVisitor $visitor, $depth = 0) {
        $visitor->visitTag($this, $depth);
        if (!empty($this->kids)) {
            foreach ($this->kids as $kid) {
                $kid->accept($visitor, $depth + 1);
            }
        }
    }

    /**
     * swordXMLtag::data()
     *
     * @param mixed $item
     * @return
     */
    public static function data($item) {
        if (is_array($item)) {
            $item = array_shift($item);
        }
        return (string) $item;
    }

    /**
     * Factory pattern to make tag nodes given name and attributes
     * it also puts in a sequence number which uniquely identifies
     * each tag in the parse.
     *
     * @param  $name The name of the tag
     * @param  $attrs Any attributes for this tag
     * @return Refernce to the created tag object
     */
    public static function &build($name, $attrs = array()) {
        static $counter = 0;
        $tag = new swordXMLtag($name, $attrs);
        $tag->count = $counter++;
        return $tag;
    }
}

/**
 * Parser object for NSOnline
 *
 * Builds a tree of tag objects from the supplied XML.
 * Note of interest is that the stack is backwards,
 * the zeroeth object is always the most recent object
 * so that we don't need an index value to know where it is.
 *
 * This also creates another structure ($tags) which, for
 * each tag name, stores a list of references to all the
 * objects with that name. You can access that list by simply
 * using the tag name as if it's a variable, where there's a
 * namespace replace the ':' with double underscore '__', so
 * accessing dc:identifer becomes $parser->dc__identifier.
 *
 * It returns an array of matching items. The index of each
 * item is its tag position.
 *
 * @param  $xml The XML as text to be parsed
 */
class swordXMLParser {
    private $xml = '';
    private $parser = NULL;
    private $stack = array();
    private $root;
    private $tags = array();

    /**
     * swordXMLParser::__construct()
     *
     * @param mixed $xml
     */
    public function __construct($xml) {
        $this->xml = $xml;

        $this->parser = xml_parser_create();
        xml_set_element_handler($this->parser, array($this, 'start'), array($this, 'finish'));
        xml_set_character_data_handler($this->parser, array($this, 'char'));
        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);
    }

    /**
     * swordXMLParser::__destruct()
     *
     */
    public function __destruct() {
        xml_parser_free($this->parser);
    }

    /**
     * Allows certain values to be accessed
     */

    public function __get($var) {
        switch ($var) {
            case 'root':
                $val = array(&$this->root);
                break;
            default:
                if (strpos($var, '__') > 0) {
                    $var = str_replace('__', ':', $var);
                }
                if (array_key_exists($var, $this->tags)) {
                    $val = $this->tags[$var];
                } else {
                    $val = NULL;
                }
                break;
        }
        return $val;
    }
    // --------------------------------------------------------------------
    /**
     * Parse the file and return the root object, at this point the
     * only thing you know is that you have a correctly formed XML
     * structure, the meaning is up to the calling routine.
     */

    public function parse() {
        if (!xml_parse($this->parser, $this->xml, TRUE)) {
            die(sprintf("XML error: %s at line %d",
                    xml_error_string(xml_get_error_code($this->parser)),
                    xml_get_current_line_number($this->parser)));
        }
        return $this->root;
    }

    /**
     * Add the next tag to the front of the stack, make it
     * a child of the current
     */

    private function start($parser, $name, $attrs) {
        $tag = swordXMLTag::build($name, $attrs);
        if (!$this->root) {
            $this->root = &$tag;
        } else {
            $this->stack[0]->kid = array(&$tag);
        }
        $this->tags[$name][] = &$tag;
        array_unshift($this->stack, $tag);
    }

    /**
     * swordXMLParser::finish()
     *
     * @param mixed $parser
     * @param mixed $name
     * @return
     */
    private function finish($parser, $name) {
        $elem = array_shift($this->stack);
    }

    /**
     * swordXMLParser::char()
     *
     * @param mixed $parser
     * @param mixed $data
     * @return
     */
    private function char($parser, $data) {
        $this->stack[0]->data .= $data;
    }
}
// =====================================================================
/**
 * Example Implementation of swordXMLVisitor class
 *
 * This is only an example because the swordXMLParser already
 * provides a much faster method of finding all of a specific
 * tag, whereas this one has to traverse the whole tree every
 * time.
 */
/*
class nsFindTags extends swordXMLVisitor {
    private $name;
    private $tags = array();

    public function __construct($name) {
        $this->name = $name;
    }

    public function visitTag(swordXMLtag $tag, $depth) {
        if ($tag->name==$this->name) {
            $this->tags[] =& $tag;
        }
    }

    public function reset($name) {
        $this->name = $name;
        $this->tags = array();
    }

    public function result() { return $this->tags; }
}
*/
// =====================================================================
